# Übersicht

![anmelden](img/loginscreen.png)

Stammesverwaltung ist eine [Django](https://www.djangoproject.com/)-Applikation für Verwaltungsaufgaben im Stamm [DPSG St. Vitus Hirschaid](https://pfadfinder-hirschaid.de).

Sie ist in die folgenden sechs Teilbereiche aufgeteilt:

* [Aktionen](Aktionen)
    * [Anmelden](Aktionen/Anmelden)
    * [Details](Aktionen/Details)
    * [Erstellen](Aktionen/Erstellen)
* [Mitglieder](Mitglieder)
    * [Mitgliederliste](Mitglieder/Mitgliederliste)
    * [SEPA-Mandate](Mitglieder/SEPA-Mandate)
    * [Mitgliedsbeitragseinzüge](Mitglieder/Mitgliedsbeitragseinzüge)
* [Finanzen](Finanzen)
    * [Lastschriften](Finanzen/Lastschriften)
    * [Abrechnungen](Finanzen/Abrechnungen)
    * [Spendenquittung](Finanzen/Spendenquittung)
    * [Buchungen](Finanzen/Buchungen)
    * [Konten](Finanzen/Konten)
* [Material](Material)
    * [Materialliste](Material/Materialliste)
    * [Bestellungen](Material/Bestellungen)
* [Benutzerkonto](Benutzerkonto)
    * [Administration](Benutzerkonto/Administration)
    * [Kontoeinstellungen](Benutzerkonto/Kontoeinstellungen)