# Spendenquittungen

## Antrag auf Spendenquittung exportieren (nur Leiter)
![finanzen_spenden](../img/127.0.0.1_8000_finanzen_spenden_2019__iPad_.png)

Hier können alle Aufwendungen, die nicht ausbezahlt werden sollen, nach Jahren getrennt eingesehen, bearbeitet und wieder gelöscht werden. Mit dem Button "Spendenquittung" wird ein Antrag auf Ausstellung einer Spendenquittung für das ausgewählte Jahr als PDF erstellt. Dieser muss jedoch noch ausgedruckt, unterschrieben und beim Freundeskreis eingereicht werden!

## Fahrtkosten anlegen (nur Leiter)
![127.0.0.1_8000_finanzen_fahrtkosten-anlegen__iPad_](../img/127.0.0.1_8000_finanzen_fahrtkosten-anlegen__iPad_.png)

Um einen Fahrtkosteneintrag anzulegen, müssen das Datum der Fahrt, die Strecke und ein Kommentar (der Grund der Fahrt) eingetragen werden. Der Empfänger muss ein [Mitglied](Mitglieder/Mitgliederliste) sein, das dem jeweiligen Benutzer zugeordnet ist. Auf dessen Name wird auch der Antrag beim Export ausgestellt!

Der Betrag wird automatisch errechnet durch die Multiplikation der angegebenen Kilometer mit 0,30 EUR.

## Aufwendung anlegen (nur Leiter)
![127.0.0.1_8000_finanzen_aufwendung-anlegen__iPad_](../img/127.0.0.1_8000_finanzen_aufwendung-anlegen__iPad_.png)

Ausgaben für den Stamm, die nicht ausbezahlt werden sollen (z. B. Druckkosten, Porto, Kleinigkeiten, etc,) können hier eingetragen werden.

Es muss das Datum, an dem die Kosten enstanden sind, sowie ein Name, der Betrag und ein Kommentar (eine Begründung) angegeben werden. Auch hier ist darauf zu achten, dass als Empfänger das Mitglied ausgewählt wird, auf das der Antrag ausgestellt werden soll!