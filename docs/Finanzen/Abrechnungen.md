# Abrechnungen

## Abrechnungen einsehen (nur Leiter)
![127.0.0.1_8000_finanzen_abrechnungen__iPad_](../img/127.0.0.1_8000_finanzen_abrechnungen__iPad_.png)

Hier können alle offenen (noch nicht durch den Kassierer überwiesenen) Abrechnungen des jeweiligen Leiters eingesehen werden. Solange eine Abrechnung noch nicht abgeschlossen ist, kann sie bearbeitet oder gelöscht werden.

Außerdem sind alle bereits abgeschlossen Abrechnungen aufgelistet.

## Abrechnung erstellen (nur Leiter)
TODO

## Abrechnung buchen (nur Kassierer & Vorstand)
![127.0.0.1_8000_finanzen_abrechnung-buchen_10__iPad_](../img/127.0.0.1_8000_finanzen_abrechnung-buchen_10__iPad_.png)

Für den Kassierer und den Vorstand sind auf der Abrechnungsseite die Abrechnungen aller Leiter sowie ein Button zum "Buchen" sichtbar. Soll eine Abrechnungen gebucht werden, wird das Formular für einen neue [Buchung](Buchungen) geöffnet, das bereits anhand der Angaben des Leiters vorausgefüllt ist. Als Buchungsdatum ist der heutige Tag angegeben.

Es sollte überprüft werden, ob der Beleg von ausreichender Bildqualität ist und mit den Angaben (insbesondere dem Gegenkonto, hier "Habenkonto") übereinstimmt. Als Sollkonto ist das Girokonto vorausgewählt.

Nach der Buchung muss der Betrag an das vom Leiter angegebene Konto überwiesen werden.