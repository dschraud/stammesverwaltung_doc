# Lastschriften

## Lastschriften einsehen
![finanzen_lastschriften](../img/127.0.0.1_8000_finanzen_lastschriften__iPad_.png)

Unter "Offene Lastschriften" steht das Abbuchungsdatum für die nächsten fälligen Lastschriften. Diese werden direkt darunter unter Angabe des Mitglieds, auf das sich die Lastschrift bezieht, sowie des verwendeten SEPA-Mandats, des Verwendungszweck und des Betrag aufgeführt.

Unter "Abgeschlossene Lastschriften" finden sich alle Lastschriften, die bereits abgebucht wurden, mit dem jeweiligen Ausführungsdatum.

## Lastschriften anlegen (nur Kassierer & Vorstand)
![finanzen_lastschrift-anlegen](../img/127.0.0.1_8000_finanzen_lastschrift-anlegen__iPad_.png)

Hier muss nur das entsprechende Mitglied ausgewählt werden und ein Verwendungszweck sowie ein Teilnehmerbeitrag eingegeben werden. Als SEPA-Mandat wird automatisch das für das Mitglied hinterlegte Mandat für Teilnehmerbeiträge verwenden. Die Lastschrift wird automatisch dem nächstmöglichen Abbuchungsdatum zugeordnet.

## Lastschriften exportieren (nur Kassierer & Vorstand)
![127.0.0.1_8000_finanzen_lastschriften__iPad___1_](../img/127.0.0.1_8000_finanzen_lastschriften__iPad___1_.png)

Hier können die abgeschlossenen Lastschriften gruppiert nach Monaten für alle Mitglieder eingesehen werden und als CSV für die Bank exportiert werden. Sobald ein neuer Monat zum Export verfügbar ist, wird der Kassierer per E-Mail benachrichtigt.
