# Finanzen

## Für Mitglieder
* Unter [Lastschriften](Lastschriften) können alle bisherigen sowie zukünftigen Lastschriften eingesehen werden.

## Für Leiter
* Unter [Abrechnungen](Abrechnungen) können Auslagen, die für den Stamm gemacht wurden, hochgeladen werden, damit sie vom Kassierer überwiesen werden.
* Unter [Spendenquittungen](Spendenquittungen) können Auslagen, die nicht ausbezahlt werden sollen, sowie entstandene Fahrtkosten hochgeladen werden, um anschließend einen Antrag für eine Spendenquittung zu erstellen.
* Unter [Buchungen](Buchungen) können alle Buchungen in der Kasse des Stammes eingesehen und exportiert werden.
* Unter [Konten](Konten) sind die Buchungen nach Gegenkonten sortiert.

## Für Kassierer & Vorstand
* Unter [Lastschriften](Finanzen) können Lastschriften erstellt werden und für alle Mitglieder eingesehen sowie exportiert werden.
* Unter [Abrechnungen](Abrechnungen) können alle Abrechnungen gebucht und bearbeitet werden.
* Unter [Buchungen](Buchungen) können Buchungen erstellt werden.
* Unter [Konten](Konten) können Konten erstellt und geschlossen werden.