# Wiki for Stammesverwaltung

[https://dschraudner.gitlab.io/stammesverwaltung_doc](https://dschraudner.gitlab.io/stammesverwaltung_doc)

## Usage

```bash
pip install mkdocs
pip install mkdocs-material
pip install pymdown-extensions
pip install pygments-style-solarized
```

```bash
mkdocs --version
```

```bash
mkdocs serve
```

## Links

* [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)